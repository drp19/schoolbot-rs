use std::collections::HashMap;
use std::sync::Arc;
use std::str::FromStr;
use serenity::{
    async_trait,
    model::prelude::*,
    prelude::*,
};
use regex::{Regex, Captures};
use tracing::{warn, info, debug, instrument};

pub struct GuildData {
    channels : HashMap<ChannelId, ChannelData>
}

#[derive(Clone, Debug)]
pub struct ChannelData {
    make_channel : bool,
    make_role : bool,
    reacts : u64,
    regex : Regex,
    category : Option<ChannelId>
}

pub struct Handler {
    data : Arc<RwLock<HashMap<GuildId, GuildData>>>
}

impl Handler {
	pub fn new() -> Handler {
        Handler { data: Arc::new(RwLock::new(HashMap::new())) }
	}
}

const ADD_EMOJI : &str = "✌️";
const DONE_EMOJI : &str = "✅";
const WARN_EMOJI : &str = "⚠";


#[async_trait]
impl EventHandler for Handler {
    /// cache: channel, guild
    /// http: create channel, role, permissions, delete, add react
    #[instrument(skip(self, ctx, msg), fields(id = %msg.id, guild = %msg.guild_id.unwrap_or(GuildId(0)), channel = %msg.channel_id))]
    async fn message(&self, ctx: Context, msg: Message) {
        debug!("Recieved message");
        let channel_data = match get_event_channel_data(&msg.guild_id, &msg.channel_id, &self.data).await {
            Some(c_d) => c_d,
            None => {
                return;
            }
        };
        let content = msg.content.to_lowercase();

        // check message content
        if !channel_data.regex.is_match(&content) {
            info!("Does not match channel syntax. Deleting.");
            if !msg.author.id.as_u64().eq(&213118265633800192) {
                if let Err(e) = msg.delete(ctx.http).await {
                    warn!(error = %e, "Error when trying to delete message.");
                }
            } else {
                info!("Not deleting.");
            }
            return;
        }
        // get guild from cache
        let guild = match msg.guild(&ctx.cache).await {
            Some (g) => g,
            None => {
                warn!("No guild obj cached.");
                return;
            }
        };
        // try to get channel and role for message
        let mut channel = guild.channel_id_from_name(&ctx, &content).await;
        let mut role = guild.role_by_name(&content).map(|r| r.id);
        
        // main processing
        debug!(channel = channel.is_some(), role = role.is_some(), "Beginning processing actions");
        if channel.is_some() && role.is_some() {
            if let Err(e) = msg.delete(ctx.http).await {
                warn!(error = %e, "Error when trying to delete message.");
            }
        } else if channel.is_none() && role.is_none() {
            if let Err(e) = msg.react(ctx.http, ReactionType::Unicode(ADD_EMOJI.to_string())).await {
                warn!(error = %e, reaction = ADD_EMOJI, "Error when trying to add reaction.");
            };
        } else {
            {
                let add_react = msg.react(&ctx.http, ReactionType::Unicode(ADD_EMOJI.to_string()));
                let done_react = msg.react(&ctx.http, ReactionType::Unicode(DONE_EMOJI.to_string()));
                if let Err(e) = tokio::try_join!(add_react, done_react) {
                    warn!(error = %e, REACTION = %format!("{}|{}", ADD_EMOJI, DONE_EMOJI), "Error when trying to add reaction.");
                }
            }
            if channel.is_some() && channel_data.make_role {
                role = match create_role(&ctx, &guild, &content).await {
                    Some(r) => Some(r.id),
                    None => {return;}
                };
            } else if role.is_some() && channel_data.make_channel {
                channel = match create_channel(&ctx, &guild, &content, &channel_data.category).await {
                    Some(c) => Some(c.id),
                    None => {return;}
                };
            } else if role.is_some() && channel_data.make_role {
                if let Err(e) = msg.react(&ctx.http,  ReactionType::Unicode(WARN_EMOJI.to_string())).await {
                    warn!(error = %e, "Error reacting to message.");
                }
            } else {
                if let Err(e) = msg.delete(&ctx.http).await {
                    warn!(error = %e, "Error deleting message.");
                }
            }
            if channel_data.make_role && channel_data.make_channel && channel.is_some() && role.is_some() {
                if !modify_channel(&ctx, guild.id, channel.unwrap(), role.unwrap()).await {
                    warn!(channel = %channel.unwrap(), "Did not modify channel!");
                }
            }
        }
        info!("Processed message successfully");
    }

    /// http: message, member, add role, if creating role, many others
    /// cache: guild
    #[instrument(skip(self, ctx, add_reaction), fields(channel = %add_reaction.channel_id, msg = %add_reaction.message_id, user = %add_reaction.user_id.unwrap_or(UserId(0)), emoji = %add_reaction.emoji))]
    async fn reaction_add(&self, ctx: Context, add_reaction: Reaction) {
        debug!("Recieved reaction_add");
        let channel_data = match get_event_channel_data(&add_reaction.guild_id, &add_reaction.channel_id, &self.data).await {
            Some(c_d) => c_d,
            None => {
                return;
            }
        };
        if add_reaction.user_id.is_none() {
            warn!("Removed reaction user not in cache.");
            return;
        }
        if add_reaction.emoji != ReactionType::Unicode(ADD_EMOJI.to_string()) {
            info!("Not add emoji.");
            return;
        }
        let message = match add_reaction.message(&ctx.http).await {
            Ok(m) => m,
            Err(e) => {
                warn!(error = %e, "Error when trying to retrieve message.");
                return;
            }
        };
        let guild = match add_reaction.guild_id.unwrap().to_guild_cached(&ctx.cache).await {
            Some (g) => g,
            None => {
                warn!("No guild obj cached.");
                return;
            }
        };
        if message.reactions.iter().find(|r| r.me && r.reaction_type == ReactionType::Unicode(DONE_EMOJI.to_string())).is_none() {
            match message.reactions.iter().find(|r| r.reaction_type == ReactionType::Unicode(ADD_EMOJI.to_string())) {
                Some(r) => {
                    if r.count >= channel_data.reacts {
                        let content = message.content.to_lowercase();
                        let mut role = guild.role_by_name(&content).map(|r| r.id);
                        let mut channel = guild.channel_id_from_name(&ctx, &content).await;
                        if role.is_none() && channel_data.make_role {
                            role = match create_role(&ctx, &guild, &content).await {
                                Some(r) => Some(r.id),
                                None => {
                                    warn!(role = %content, "Could not create role, finishing.");
                                    return;
                                }
                            }
                        }
                        if channel.is_none() && channel_data.make_channel {
                            channel = match create_channel(&ctx, &guild, &content, &channel_data.category).await {
                                Some(c) => Some(c.id),
                                None => {
                                    warn!(channel = %content, "Could not create channel, finishing.");
                                    return;
                                }
                            }
                        }
                        if channel_data.make_channel && channel_data.make_role {
                            if !modify_channel(&ctx, guild.id, channel.unwrap(), role.unwrap()).await {
                                warn!("Could not modify channel, continuing but bad!!");
                            }
                        }
                        if let Err(e) = message.react(&ctx.http, ReactionType::Unicode(DONE_EMOJI.to_string())).await {
                            warn!(msg = %message.id, error = %e, "Could not react done.");
                        }
                        if channel_data.make_role {
                            match message.reaction_users(&ctx.http, ReactionType::Unicode(ADD_EMOJI.to_string()), Some(100), None).await {
                                Ok(users) => {
                                    info!(num = users.len(), "Adding roles for users");
                                    for user in users {
                                        match guild.member(&ctx.http, user.id).await {
                                            Ok(mut m) => {
                                                if let Err(e) = m.add_role(&ctx.http, role.unwrap()).await {
                                                    warn!(member = %user.id, error = %e, "Error when trying to add role.");
                                                    return;
                                                }
                                            }
                                            Err(e) => {
                                                warn!(member = %user.id, error = %e, "Error when trying to retrieve member.");
                                                return;
                                            }
                                        }
                                    }
                                }
                                Err(e) => {
                                    warn!(error = %e, "Could not retrieve reaction users.");
                                }
                            }
                        }
                    } else {
                        info!(reacts = %r.count, "Not enough reacts.");
                        return;
                    }
                },
                None => {
                    warn!("No add reactions on message.");
                    return;
                }
            }
        } else if channel_data.make_role {
            let role = match guild.role_by_name(&message.content.to_lowercase()) {
                Some(r) => r,
                None => {
                    warn!("Could not get role for message.");
                    return;
                }
            };
            match guild.member(&ctx.http, add_reaction.user_id.unwrap()).await {
                Ok(mut m) => {
                    if let Err(e) = m.add_role(&ctx.http, role.id).await {
                        warn!(error = %e, "Error when trying to add role.");
                        return;
                    }
                }
                Err(e) => {
                    warn!(error = %e, "Error when trying to retrieve member.");
                    return;
                }
            }
        }
        info!("Reaction processed successfully.");
    }

    /// http: message, member, remove role
    /// cache: guild
    #[instrument(skip(self, ctx, removed_reaction), fields(channel = %removed_reaction.channel_id, msg = %removed_reaction.message_id, user = %removed_reaction.user_id.unwrap_or(UserId(0)), emoji = %removed_reaction.emoji))]
    async fn reaction_remove(&self, ctx: Context, removed_reaction: Reaction) {
        debug!("Recieved reaction_remove");
        let channel_data = match get_event_channel_data(&removed_reaction.guild_id, &removed_reaction.channel_id, &self.data).await {
            Some(c_d) => c_d,
            None => {
                return;
            }
        };
        if removed_reaction.user_id.is_none() {
            warn!("Removed reaction user not in cache.");
            return;
        }
        if removed_reaction.emoji != ReactionType::Unicode(ADD_EMOJI.to_string()) {
            info!("Not add emoji.");
            return;
        }
        let message = match removed_reaction.message(&ctx.http).await {
            Ok(m) => m,
            Err(e) => {
                warn!(error = %e, "Error when trying to retrieve message.");
                return;
            }
        };
        let guild = match removed_reaction.guild_id.unwrap().to_guild_cached(&ctx.cache).await {
            Some (g) => g,
            None => {
                warn!("No guild obj cached.");
                return;
            }
        };
        // if want role and bot has already reacted (done)
        if channel_data.make_role && message.reactions.iter().find(|r| r.me && r.reaction_type == ReactionType::Unicode(DONE_EMOJI.to_string())).is_some() {
            let role = match guild.role_by_name(&message.content.to_lowercase()) {
                Some(r) => r,
                None => {
                    warn!(role = %message.content.to_lowercase(), "Could not get role for message.");
                    return;
                }
            };
            match guild.member(&ctx.http, removed_reaction.user_id.unwrap()).await {
                Ok(mut m) => {
                    if let Err(e) = m.remove_role(&ctx.http, role.id).await {
                        warn!(error = %e, "Error when trying to remove role.");
                        return;
                    }
                }
                Err(e) => {
                    warn!(error = %e, "Error when trying to retrieve member.");
                    return;
                }
            }
        }
        info!("Reaction processed successfully.");
    }

    /// relies on buggy pins endpoint but only is called once per route so it's fine
    /// http: channel pins
    #[instrument(skip(self, ctx, guild, _is_new), fields(guild = %guild.id))]
    async fn guild_create(&self, ctx: Context, guild: Guild, _is_new: bool) {
        debug!("Recieved guild_create");
        // find admin channel
        let channel = match guild.channels.values().find(|channel| channel.name == "admin") {
            Some(c) => c,
            None => {
                warn!("Joined guild has no admin channel. Ignoring.");
                return;
            }
        };
        debug!(channel = %channel.id, "proessing admin channel");
        let mut pins = match channel.pins(&ctx.http).await {
            Ok(pins) => pins,
            Err(e) => {
                warn!(error = %e, "Error when trying to retrieve pins.");
                return;
            }
        };
        // newest items first
        pins.sort_unstable_by(|a, b| b.timestamp.cmp(&a.timestamp));
        let config_message = match pins.iter().find(|message| message.content.starts_with("schoolbot_config")) {
            Some(msg) => msg,
            None => {
                warn!(channel = %channel.id, "No valid config message found.");   
                return;
            }
        };
        process_config_message(&ctx, config_message, &guild, &self.data).await;
    }

    /// when a pinned message is updated (whether from being pinned or otherwise), if it is in an admin guild channel,
    /// try to process it, otherwise ignore it
    /// http: message get
    /// cache: channel, guild
    /// other: MessageUpdateEvent has guild_id and pinned fields when it is supposed to
    #[instrument(skip(self, ctx, _new, event), fields(message_id = %event.id, channel_id = %event.channel_id))]
    async fn message_update(&self, ctx: Context, _old_if_available: Option<Message>, _new: Option<Message>, event: MessageUpdateEvent) {
        debug!("Recieved message_update");
        if !event.pinned.unwrap_or(false) {
            info!("Edit not in a pinned message.");
            return;
        }
        if event.guild_id.is_none() {
            info!("Edit not in guild (DM?).");
            return;
        } 
        let msg = match event.channel_id.message(&ctx.http, event.id).await {
            Ok(m) => m,
            Err(e) => {
                warn!(error = %e, "Error retrieving message");
                return;
            }
        };
        match msg.channel(&ctx.cache).await {
            Some(Channel::Guild(c)) => {
                if c.name != "admin" {
                    info!("Edit not in admin channel.");
                    return;
                }
            }
            None => {
                warn!("Could not get channel from cache.");
                return;
            }
            _ => {
                warn!("Message not guildchannel???");
                return;
            }
        }
        let guild = match event.guild_id.unwrap().to_guild_cached(&ctx.cache).await {
            Some(g) => g,
            None => {
                warn!("Could not get guild from cache.");
                return;
            }
        };
        process_config_message(&ctx, &msg, &guild, &self.data).await;
    }

    async fn ready(&self, _ctx: Context, ready: Ready) {
        info!(username = %ready.user.name, "Connected");
        for guild in ready.guilds.iter() {
            debug!(id = %guild.id(), "guild seen")
        }
        //ctx.cache.set_max_messages(10).await;
    }
}

/// cache: channel/category
/// http: channel
#[instrument(skip(ctx, config_message, guild, data), fields(channel = %config_message.channel_id, msg = %config_message.id))]
async fn process_config_message(ctx : &Context, config_message : &Message, guild : &Guild, data : &Arc<RwLock<HashMap<GuildId, GuildData>>>) -> bool {
    lazy_static! {
        static ref LINE_REGEX: Regex = Regex::new("^([a-z-]+), (true|false), (true|false), ([0-9]{1,2}), (.*), ([a-z-]+)$").unwrap();
    }
    debug!("Processing config message");
    let mut channels: HashMap<ChannelId, ChannelData> = HashMap::new();
    for line in config_message.content.split("\n") {
        if line.starts_with("schoolbot_config") || line.starts_with("#") {
            continue;
        }
        let captures : Option<Captures> = LINE_REGEX.captures(line);
        match captures {
            Some(c) => {
                let channel_id : ChannelId;
                let mut category_id : Option<ChannelId> = None; // only ever set once even though it's mutable
                {
                    let finding_category = !c.get(6).unwrap().as_str().eq("none");

                    let channel_fut = guild.channel_id_from_name(&ctx.cache, c.get(1).map_or("", |m| m.as_str()));
                    let category_fut = guild.channel_id_from_name(&ctx.cache, c.get(6).map_or("", |m| m.as_str()));
                    let (channel_opt, category_opt) = tokio::join!(channel_fut, category_fut) ;
                    if channel_opt.is_none() {
                        warn!(line, "Channel of that name does not exist");
                        return false;
                    }
                    if finding_category && category_opt.is_none() {
                        warn!(line, "Category of that name does not exist");
                        return false;
                    }
                    if finding_category {
                        match tokio::join!(ctx.http.get_channel(channel_opt.unwrap().0), ctx.http.get_channel(category_opt.unwrap().0)) {
                            (Err(e), _) => {
                                warn!(line, channel = %channel_opt.unwrap(), error = %e, "Error when trying to retrieve channel.");
                                return false;
                            }
                            (_, Err(e)) => {
                                warn!(line, category = %channel_opt.unwrap(), error = %e, "Error when trying to retrieve category.");
                                return false;
                            }
                            (Ok(Channel::Guild(channel)), Ok(Channel::Category(_))) if channel.kind == ChannelType::Text => {
                                channel_id = channel_opt.unwrap();
                                category_id = category_opt;
                            }
                            (_,_) => {
                                warn!(line, channel = %channel_opt.unwrap(), category = %channel_opt.unwrap(), "Not a guild text channel or category.");
                                return false;
                            }
                        }
                    } else {
                        match ctx.http.get_channel(channel_opt.unwrap().0).await {
                            Ok(Channel::Guild(channel)) if channel.kind == ChannelType::Text => {channel_id = channel_opt.unwrap();}
                            Err(e) => {
                                warn!(line, error = %e, "Error when trying to retrieve channel.");
                                return false;
                            }
                            Ok(_) => {
                                warn!(line, channel = %channel_opt.unwrap(), "Not a guild text channel.");
                                return false;
                            }
                        }
                    }
                }
                if channels.get(&channel_id).is_some() {
                    warn!(line, channel = %channel_id, "Duplicate channel.");
                    return false;
                }
                let regex_match = match Regex::new(c.get(5).map_or("", |m| m.as_str())) {
                    Ok(r) => r,
                    Err(e) => {
                        warn!(line, err = %e, "Not a valid regex.");
                        return false;
                    }
                };
                // can safely unwrap these since the regex verifies it
                let want_role = bool::from_str(c.get(2).unwrap().as_str()).unwrap();
                let want_channel = bool::from_str(c.get(3).unwrap().as_str()).unwrap();
                if !(want_role || want_channel) {
                    warn!(line, "Need to want channel or role.");
                    return false;
                }
                channels.insert(channel_id, ChannelData{
                    make_channel: want_channel,
                    make_role: want_role,
                    reacts: u64::from_str(c.get(4).unwrap().as_str()).unwrap(),
                    regex: regex_match,
                    category: category_id
                });
            },
            None => {
                warn!(line, "Invalid line in config message.");
                return false;
            }
        }
    }
    let mut map = data.write().await;
    map.insert(guild.id, GuildData { channels: channels});
    info!("Successful config update.");
    true
}

/// returns None on http error
#[instrument(skip(ctx, guild) fields(guild = %guild.id))]
async fn create_role(ctx: &Context, guild: &Guild, name: &str) -> Option<Role> {
    debug!("Creating role");
    let res = match guild.create_role(&ctx.http, |r| r.permissions(Permissions::empty()).name(name)
    .mentionable(true)).await {
        Ok(r) => Some(r),
        Err(e) => {
            warn!(error = %e, "Error when trying to create role.");
            None
        }
    };
    info!("Role creation successful.");
    res
}

/// returns None on http error
#[instrument(skip(ctx, guild) fields(guild = %guild.id))]
async fn create_channel(ctx: &Context, guild: &Guild, name: &str, category: &Option<ChannelId>) -> Option<GuildChannel> {
    debug!("Creating channel");
    let res = match guild.create_channel(&ctx.http, |c|
        if category.is_some() {c.name(name).category(category.unwrap())} else {c.name(name)}
    ).await {
        Ok(c) => Some(c),
        Err(e) => {
            warn!(error = %e, "Error when trying to create channel.");
            None
        }
    };
    info!("Channel creation successful");
    res
}

/// returns false on http error
#[instrument(skip(ctx))]
async fn modify_channel(ctx: &Context, guild: GuildId, channel: ChannelId, role: RoleId) -> bool {
    let perm1 = PermissionOverwrite {
        allow: Permissions::SEND_MESSAGES | Permissions::READ_MESSAGES | Permissions::MANAGE_MESSAGES,
        deny: Permissions::empty(),
        kind: PermissionOverwriteType::Role(role)
    };
    let perm2 = PermissionOverwrite {
        deny: Permissions::SEND_MESSAGES | Permissions::READ_MESSAGES,
        allow: Permissions::empty(),
        kind: PermissionOverwriteType::Role(RoleId(guild.0))
    };
    debug!(?perm1, ?perm2, "Modifying channel permissions");
    let p1 = channel.create_permission(&ctx.http, &perm1).await;
    let p2 = channel.create_permission(&ctx.http, &perm2).await;
    if let Err(e) = p1.and(p2) {
        warn!(error = %e, "Error when trying to modify channel.");
        return false;
    };
    info!("Successfully modified channel permissions");
    true
}

/// no cache/http dependency
#[instrument(skip(data, guild_id))]
async fn get_event_channel_data(guild_id : &Option<GuildId>, channel_id : &ChannelId, data : &Arc<RwLock<HashMap<GuildId, GuildData>>>) -> Option<ChannelData> {
    debug!("Beginning channel data retrieval.");
    // ensure guild message
    if guild_id.is_none() {
        info!("Message was not in a guild.");
        return None;
    }
    // get metadata, if it exists
    let channel_data = match data.read().await.get(&guild_id.unwrap()) {
        Some (d) => match d.channels.get(channel_id) {
            Some(data) => data.clone(),
            None => {
                info!("No channel binding.");
                return None;
            }
        }
        None => {
            info!("No guild binding. Make sure the guild has a config message.");
            return None;
        }
    };
    debug!("Retrieved channel data successfully.");
    Some(channel_data)
}