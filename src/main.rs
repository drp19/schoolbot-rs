use std::env;
use serenity::prelude::*;
use tracing::{instrument, error};
#[macro_use] extern crate lazy_static;

mod event_handlers;

/// Recommend running with RUST_LOG=INFO,schoolbot_rs=DEBUG
#[tokio::main]
#[instrument]
async fn main() {
    //std::env::set_var("RUST_LOG", "INFO,schoolbot_rs=DEBUG");
    tracing_subscriber::fmt::init();
    // read credentials
    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment");
    // validate token
    if let Err(e) = serenity::client::validate_token(&token) {
		error!("FATAL: Provided token is invalid! Debug: {:?}", e);
    }

    // Create a new instance of the Client, logging in as a bot. This will
    // automatically prepend your bot token with "Bot ", which is a requirement
    // by Discord for bot users.
    let mut client = Client::builder(&token)
        .event_handler(event_handlers::Handler::new())
        .await
        .expect("FATAL: Err creating client");
	
	// start a shard
	if let Err(e) = client.start().await {
		error!(error = %e, "Error occured creating shard.");
	}

}
